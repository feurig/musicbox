# MusicBox.
I found an old style radio and replaced its innards with an old raspberry pi with a 7" monitor, a 2w amp,
and an i2c controlled FM radio module. The idea was to create an appliance that worked exactly like a radio. 

*  You turn it on with the volume/on dial. 
*  The volume knob works as expected
*  You tune it with the frequency knob
*  A dial or meter shows the frequency 
*  When you turn it off it doubles as a clock. 

There are a lot of possibilities with this framework. 

* You could use the "Musicbox" as an airplay device.
* You could make a streaming server that emulates a radio (Shirleys idea: map radio stations to their digital streams)
* You could stream the local radio to anywhere.
* You could (in theory) replace the cheap/ugly radio that your girlfriend uses to listen to npr every morning. 
* You could make an alarm clock that does these as well.

## Technologies/Frameworks
### Raspberry Pi 1
#### Raspbian
##### Setup
#### 7" HDMI Monitor
#### Alternate Backing Camera Monitor.
#### PAM8610 10W amp.
#### Adafruit Feather M0 
The knobs and buttons from the original radio are read by an adafruit feather M0. The fm dial was replaced by a multi turn potentiometer.
![Physical Interface][schematic]
[schematic]: https://bitbucket.org/feurig/musicbox/raw/0bcb5562365681f7e4b6c7f1f769f31cd4c05ffd/hardware/UI_schem.jpg
##### platformio/arduino
I programmed the M0 using platformio. It reads all of the io, filters out (most of) the noise and sends it blindly to the serial port. 
##### ser2net
I use ser2net to take the serial data and send it out over tcp/ip. One of the advantages of this is that it dramatically simplifies connecting to the device and it also makes it possible read the devices remotely.

#### Griffin iMic / USB sound card
Any usb audio card will do but for audio quality the iMic is hard to beat. 
#### TEA5767 FM Radio module
Phillips puts out an I2C controlled FM radio reciever module. The boards purchade pumped the audio to a headphone jack which is connected to the line/mic input for the imic. I could have controlled it using the microcontroller but instead I attached the 12c connection to the pi
### Python3
None of the code here is python 2/x aware or tested.
#### Wx-Python
It takes 2 days to build wx-python on the pi-1 but it does what we need it to.
##### pubsub
wxpython recommends pubsub as a messeging system. You have to manually get it though (doesn't come with wx or in a .deb) pip is your friend.  
#### alsa
On the pi the volume is controlled via alsa.
#### configparser
Settings and state information are stored using the configparser that is build into python3.
#### I2C / FM Radio Python Module
There are several modules out there a few of which work. The one I selected works but its an ugly hack that needs to be rewritten.
#### Parts used ####
(links to ebay items)

* [DC-DC 24V-12V to 5V 5A Power Supply](https://www.ebay.com/itm/DC-DC-24V-12V-to-5V-5A-Step-Down-Power-Supply-Buck-Converter-USB-Charging-Module-/133096260736)
* [PAM8610 2X15W Digital Audio Amplifier Board](https://www.ebay.com/itm/2Pcs-PAM8610-2X15W-Stereo-Digital-Audio-Amplifier-Board-Two-Channel-High-Power/122128975538)
* [TEA5767 FM Stereo Radio Module](https://www.ebay.com/itm/TEA5767-FM-Stereo-Radio-Module-With-Free-Cable-Antenna-for-Arduino-Raspberry-Pi/302331684501)
* [7" LCD Monitor](https://www.ebay.com/itm/7-inch-LCD-Screen-Display-Monitor-for-Raspberry-Pi-Driver-Board-HDMI-VGA-2AV-/264213555974?hash=item3d845aeb06)
* [Car Backup Camera 4.3" TFT/LCD Monitor](https://www.ebay.com/itm/Car-Rear-View-System-Backup-Reverse-Camera-Night-Vision-4-3-TFT-LCD-Monitor-KM/113459308689)
