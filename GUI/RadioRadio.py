#!/usr/bin/env python3
'''
-------------------------------------------------------------------RadioRadio.py
This presents the ui for a radio with a couple of modes (Applications)
The controls (mode/volume/radio frequency etc) are read using a tcp stream.

Usage: python3 GUI/RadioRadio.py [--config]
Author: Donald Delmar Davis
Copyleft (Simplified BSD) 2019 Donald Delmar Davis <don@suspectdevices.com>

Todo:
Write gui based controls (rewrite theRemote.py to be a "server").

Map notebook numbers to named applications

Write Speaker (Streaming Radio) Application.
Flesh out menus on osx.

'''
import wx
import wx.lib.agw.speedmeter as SM
import wx.lib.analogclock as ac
from math import pi
import os
import sys
from pubsub import pub
import socket
import time
import configparser
import argparse

'''----------------------------------------------------------applicationSettings

     The applicationSettings class is more or less a state table which leverages
configparser to save and restore settings.

'''

class applicationSettings(configparser.ConfigParser):
    def __init__(self, *args, **kwds):
        configparser.ConfigParser.__init__(self, *args, **kwds)
        self.defaultConfig = dict(
                {'DEFAULT':{'current.application':'4',
                            'power.switch':'0',
                            'mode.switch':'3',
                            'speaker.volume':'12.0',''
                            'remote.port':'9998',
                            'remote.ip':'127.0.0.1',
                            'remote.timeout':'0.4',
                            'left.margin':'180',
                            'window.size':'480'},

                 'radioApplication':{'radio.frequency':'88.1'}
                })
        self.loadDefaults()
        # read previous config file over defaults.
        self.readSettingsFile()

    def loadDefaults(self):
        for section in self.defaultConfig.keys() :
            self.remove_section(section)
            self[section]={}
            for k,v in self.defaultConfig[section].items() :
                self.set(section,k,v)

    def readSettingsFile(self):
        try:
            mypath=os.path.dirname(os.path.realpath(__file__))
            self.read(mypath+'/.RadioRadio.conf')
            print ("Read settings from "+mypath+'/.RadioRadio.conf')
        except Exception as e:
            print ("OMFG!!!! \nthere was a problem reading previous config file\n:", str(e))

    def writeSettingsFile(self):
        try:
            mypath=os.path.dirname(os.path.realpath(__file__))
            with open(mypath+'/.RadioRadio.conf', 'w') as configfile:
                    self.write(configfile)
            print ("Wrote settings to "+mypath+'/.RadioRadio.conf')
        except:
            print ("OMFG!!!! \nthere was a problem writing the config file\n:", str(e))

'''------------------------------------------------------------Globals/Constants

       .... I am sure that i am violating some class feng sway here....

'''

config=applicationSettings()

try:
  import alsaaudio as audio
  ThisSystemHasAlsa = True
except (ImportError, RuntimeError):
  ThisSystemHasAlsa = False

try:
    from tea5767stationscanner import tea5767
    AttachedFMRadio=tea5767() #fix shorname global foo.
    ThisSystemHasAtea5767 = True
except (ImportError, RuntimeError, OSError):
    ThisSystemHasAtea5767 = False

'''
EMS2 to internal pub/sub.
The "physical" controls interface sends data lines in the following format(EMS2).
    KWRD[!|:|?]TIMESTMP:<data>\n
    This table translates the EMS2 KeyWoRDs to the internal pub/sub messeges
    The data is sent directly along.
'''
longnames={}
longnames['KFRQ']='radio.frequency'
longnames['SPWR']='power.switch'
longnames['KVOL']='speaker.volume'
longnames['SMOD']='mode.switch'
longnames['SYNC']='sync.requested'
longnames['ACKN']='sync.acknowledged'

#APP_LEFT_MARGIN = 180 #consider moving this to config
#APP_WINDOW_SIZE = 480 #consider moving this to config





'''-------------------------------------------------------------ApplicationPanel

Make sure that the panel size and other styles are consistent between apps.

'''

class ApplicationPanel(wx.Panel):
    def __init__(self, *args, **kwds):

        wx.Panel.__init__(self,*args,**kwds)
        self.SetMinSize((593, 438)) #fixme: Hardcode foo.
        self.SetBackgroundColour(wx.Colour(0, 0, 0))
        self.SetForegroundColour(wx.Colour(255, 255, 255))

'''-----------------------------------------------------------FMRadioApplication

This App Presents the current frequency of for the FMRadio.
Basically emulates a radio dial.

'''
class FMRadioApplication(ApplicationPanel):

    def __init__(self, *args, **kwds):

        #  would rather have this upstream.....
        self.setting=config['radioApplication']
        ApplicationPanel.__init__(self,*args,**kwds)
        freqSizer = wx.BoxSizer(wx.HORIZONTAL)
        #FMControls = wx.BoxSizer(wx.VERTICAL)
        #FMControls.Add((100, 100), 1, 0, 0)
        freqSizer.Add( (int(config['DEFAULT']['left.margin']), 1 ), 0, 0, 0)
        freq = SM.SpeedMeter(self, size=(int(config['DEFAULT']['window.size']),
                                         int(config['DEFAULT']['window.size'])),
                             agwStyle=SM.SM_DRAW_HAND
                                      |SM.SM_DRAW_SECTORS
                                      |SM.SM_DRAW_MIDDLE_TEXT
                                      |SM.SM_DRAW_SECONDARY_TICKS
                            )
        self.freq=freq
        # Set The Region Of Existence Of freqMeter (Always In Radians!!!!)
        #        freq.SetAngleRange(-5*pi/4,5*pi/4)
        freq.SetAngleRange(-13.5*pi/6,-4.5*pi/6)
        #        freq.SetAngleRange(-2*pi,-1*pi)
        istart=86
        istop=109
        istep=2
        intervals = range(istart, istop, istep)
        freq.SetIntervals(intervals)
        colours = [wx.BLACK]*int((abs(istop-istart)/istep))
        freq.SetIntervalColours(colours)
        ticks = ['      '+str(interval)+'.00' for interval in intervals]
        freq.SetTicks(ticks)
        freq.SetTicksColour(wx.WHITE)
        freq.SetNumberOfSecondaryTicks(10)
        freq.SetTicksFont(wx.Font(12, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL))
        #freq.SetMiddleText(str(self.setting['radio.frequency'])+' FM')
        freq.SetMiddleTextColour(wx.WHITE)
        freq.SetMiddleTextFont(wx.Font(34, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD))
        freq.SetHandStyle("Arrow")
        freq.SetHandColour(wx.Colour(255, 50, 55))
        freq.DrawExternalArc(False)
        freq.SetSpeedBackground(wx.BLACK)
        freqSizer.Add(freq, 1, wx.ALIGN_TOP | wx.ALIGN_LEFT, 0)
        freqSizer.Add((10,10), 1, wx.EXPAND, 0)
        self.SetSizer(freqSizer)
        freq.SetSpeedValue(float(self.setting['radio.frequency']))
        if ThisSystemHasAtea5767:
            self.radio=AttachedFMRadio
        pub.subscribe(self.SetFMFrequency, "radio.frequency")

    def SetFMFrequency(self,data):
        if (float(self.setting['radio.frequency'])!=float(data)):
            self.setting['radio.frequency']=str(data)
            print ("frequency=",str(self.setting['radio.frequency'])+' FM')
            #self.freq.SetMiddleText(str(self.setting['radio.frequency'])+' FM')
            self.freq.SetSpeedValue(float(self.setting['radio.frequency']))
            if ThisSystemHasAtea5767:
                self.radio.writeFrequency(float(self.setting['radio.frequency']),0,0)

            self.freq.Update()    #self.Refresh()???


'''-------------------------------------------------------------ClockApplication

When the radio is off present a clock.

'''

class ClockApplication(ApplicationPanel):
    def __init__(self, *args, **kwds):

        ApplicationPanel.__init__(self,*args,**kwds)
        clockSizer = wx.BoxSizer(wx.HORIZONTAL)

        clockSizer.Add( (int(config['DEFAULT']['left.margin']),1), 0, 0, 0)

        clock = ac.AnalogClock(self, size=(int(config['DEFAULT']['window.size']),
                                           int(config['DEFAULT']['window.size'])),
                                hoursStyle=ac.TICKS_HEX,
                                clockStyle=ac.SHOW_HOURS_TICKS| \
                                           ac.SHOW_HOURS_HAND| \
                                           ac.SHOW_MINUTES_HAND| \
                                           ac.SHOW_SHADOWS)
        colour = wx.Colour(32, 96,32 )
        clock.SetForegroundColour(colour)
        colour = wx.Colour(0, 64, 32)
        clock.SetShadowColour(colour)
        clock.SetTickFont(wx.Font(14, wx.FONTFAMILY_MODERN, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD))
        clock.SetBackgroundColour(wx.BLACK)
        clock.SetFaceBorderColour(wx.BLACK)
        clock.SetFaceFillColour(wx.BLACK)
        clockSizer.Add(clock, 1, wx.ALIGN_TOP | wx.ALIGN_LEFT, 0)

        theTime = wx.StaticText(self, wx.ID_ANY, "      ")
        clockSizer.Add(theTime, 1, wx.EXPAND, 0)

        self.SetSizer(clockSizer)

'''-----------------------------------------------------------SpeakerApplication
....Stubb....
Since we are running the radio and the speaker through the rpi, we may as well
allow streaming sound through Airplay.
# \'''
class SpeakerApplication(ApplicationPanel):
    def __init__(self, *args, **kwds):
        # application variables here.
        #
        #
        ApplicationPanel.__init__(self,*args,**kwds)
        speakerSizer = wx.BoxSizer(wx.HORIZONTAL)
        speakerSizer.Add((APP_LEFT_MARGIN, APP_LEFT_MARGIN), 0, 0, 0)

        mypath=os.path.dirname(os.path.realpath(__file__))
        print (mypath)
        bitmap_2 = wx.StaticBitmap(self, wx.ID_ANY, wx.Bitmap(mypath +"/placeholder.png",
                                   wx.BITMAP_TYPE_ANY),size=(APP_WINDOW_SIZE,APP_WINDOW_SIZE)
                                   )
        speakerSizer.Add(bitmap_2, 1, wx.ALIGN_TOP | wx.ALIGN_LEFT, 0)
        speakerLabel = wx.StaticText(self, wx.ID_ANY, "Speaker Mode")
        speakerSizer.Add(speakerLabel, 1, wx.EXPAND, 0)

        self.SetSizer(speakerSizer)

'''


class MusicBox(wx.Frame):
    def __init__(self, *args, **kwds):
        # add interaction between applications here.
        #
        #
        kwds["style"] = kwds.get("style", 0) | wx.DEFAULT_FRAME_STYLE
        wx.Frame.__init__(self, *args, **kwds)
        self.SetSize((800, 600))

        self.applications = wx.Simplebook(self, wx.ID_ANY, style=0)
        self.application_FM_Radio = FMRadioApplication(self.applications, wx.ID_ANY)
        self.application_Clock = ClockApplication(self.applications, wx.ID_ANY)
        #self.application_Speaker = SpeakerApplication(self.applications, wx.ID_ANY)

        self.setting=config['DEFAULT']
        self.setVolume=doNothing
        self.__line_buffer=''
        self.LayoutMainWindow()
        self.Bind(wx.EVT_IDLE, self.OnIdle)
        self.Bind(wx.EVT_KEY_DOWN, self.OnKeyDown)
        self.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGED, self.OnNotebookChanged, self.applications)
        self.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGING, self.OnNotebookChanging, self.applications)


        pub.subscribe(self.PowerSwitch, "power.switch")
        pub.subscribe(self.ModeSwitch, "mode.switch")
        pub.subscribe(self.SpeakerVolume, "speaker.volume")
        # end wxGlade

    def SelectApplication(self,data):

        newMode = str(data)
        # print ( "SelectApplication ->", newMode)

        if ( self.setting['current.application']!= newMode ):
            self.setting['current.application'] = newMode
        appnumber = int(self.setting['current.application'])
        if (self.applications.GetSelection() != appnumber) :
            # if current application is 'settings' send messege "update.settings"
            self.applications.ChangeSelection(appnumber)
            print("Selecting Application #",self.setting['current.application'])
            config.writeSettingsFile()


    def PowerSwitch(self,data):
        newState = str(data)
        # print ( "powerswitch ->", newState)
        if (self.setting['power.switch'] != newState):
            self.setting['power.switch'] = newState
            if ( self.setting['power.switch']=='1' ):
                if (self.setting['mode.switch']=='2'):
                    self.SelectApplication('2')
                else:
                    self.SelectApplication('0')
            else:
                self.SelectApplication('1')

    def ModeSwitch(self,data):
        newState = str(data)
        if (self.setting['mode.switch'] != newState):
            self.setting['mode.switch'] = newState
            if ( self.setting['power.switch']=='1' ):
                if (self.setting['mode.switch']=='2') :
                    self.SelectApplication('2') #speaker
                else:
                    self.SelectApplication('0') #radio
            else:
                self.SelectApplication('1')     #clock

    def SpeakerVolume(self,data):
        if(self.setting['power.switch']=='0'):
            newVolume = '0.0'
        else:
            newVolume = str(data)
        if (self.setting['speaker.volume'] != newVolume):
            self.setting['speaker.volume'] = newVolume
            print ("New Volume: ",self.setting['speaker.volume'])
        self.setVolume(float(self.setting['speaker.volume']))


    def LayoutMainWindow(self):
        self.SetTitle("Music Box")
        applicationSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.applications.AddPage(self.application_FM_Radio, "Fm Radio Mode")
        self.applications.AddPage(self.application_Clock, "Clock Mode")
        # self.applications.AddPage(self.application_Speaker, "Speaker Mode")
        applicationSizer.Add(self.applications, 1, wx.EXPAND, 0)
        self.SetSizer(applicationSizer)
        self.Layout()

    def OnKeyDown(self,event):
        if event.GetKeyCode() == wx.WXK_CONTROL_Q:
            self.Close()

    def OnNotebookChanged(self, event):  # wxGlade: MusicBox.<event_handler>
        pub.sendMessage("save.settings", data=1)
        event.Skip()

    def OnNotebookChanging(self, event):  # wxGlade: MusicBox.<event_handler>
        #pub.sendMessage("update.settings", data=1)
        event.Skip()


    def OnIdle(self,event):
        '''---------------------------------------------------------------OnIdle
        Read Data from physical interface.
        The state of the Power/volume, Radio Frequency, and Mode dials is
        sent via a tcp stream in the format

            XXXXvTTTTTTTTddd...ddd\n

        where:
            X -> a 4 character keyword
            v -> a verb ['!'=set,':'=state,'?'=get]
            T -> a hex representation of unixtime
            : -> a delimiter
            d -> the data

        we cant use threading (on raspbian) to put this in the background
        and if we update the ui inbetween reads then it takes a visibly
        laggy amount of time to get back to reading
        ....so....
        read all avaliable data with a short settimeout
        save only newest values and send them after we are done reading
        '''
        line=self.__line_buffer
        updates={}
        self.__I_have_remote=False
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            try:
                s.connect((self.setting['remote.ip'],int(self.setting['remote.port'])))
                s.settimeout(float(self.setting['remote.timeout']))
                self.__I_have_remote=True

                while True:
                    char = s.recv(1).decode('ASCII')
                    if char=='':
                        break
                    if char != "\n":
                        line+=char
                    elif char == "\n":
                        if (len(line)>13) and ("Port already in use" not in line):
                            kwd=line[:4]
                            verb=line[4]
                            timestamp=line[5:13]
                            info=line[14:]
                            print ("Recieved",kwd,verb,timestamp,info)
                            if (kwd == 'SYNC'):
                                response="ACKN:%0.1X:%s" % (int(time.time()),info) #identity/ssn
                            elif (kwd in longnames):
                                # print(longnames[kwd],"->",info)
                                updates[longnames[kwd]]=info
                                response="%4s:%0.1X:%s" % (kwd,int(time.time()),info)
                            else:
                                response="%4s:%0.1X:%s-Not-Implimented" % ("NACK",int(time.time()),kwd)

                        #else: # short read
                        #    pass # or bitch about it here
                        #s.sendall(response.encode('ASCII'))
                        line=''
            except OSError as e:
                error=e
                if "timed out" in str(e):
                    print(".",)
                    pass # ignore timeouts
                else:
                    pub.sendMessage("power.switch", data=3) #force update of power.switch
                    pub.sendMessage("power.switch", data=0)
                    if self.__I_have_remote:
                        print("OSERROR: reading remote (" ,self.__I_have_remote,")", str(e))
                        self.__I_have_remote=False
                        return
                    pass
            except Exception as e:
                print("UNKNOWN: reading remote ",str(e))
            finally:
                s.close() #should not need this in "with" block
        self.__line_buffer=line
        for k, v in updates.items():
            pub.sendMessage(k, data=v)

        #print (line)
# end of class MusicBox

def doNothing(volume):
    '''
    Stubb for systems with no volume controls
    '''
    #print ("Set Volume = ",volume,"? WHATEVER!  \r\n I'M DOING NOTHING")
    pass

def setAlsaVolume(volume):
    '''
    Set system volume using alsaaudio.

    '''

    print ("Set Alsa Volume: ",volume)
    #m = audio.Mixer('PCM')
    m = audio.Mixer('PCM', cardindex=1) #  FIXME:  hardcode foo.
    oldVol = m.getvolume()[0]
    newVol = int((volume/11.0)*100)
    m.setvolume(newVol)
    if newVol<0.1:
        m.setmute(1)
    else:
        m.setmute(0)
    print (oldVol,"->",m.getvolume()[0])


'''---------------------------------------------------------------SettingsDialog

The settings dialog is a modal dialog to change settings or preferences.
This can be invoked using the --config option or can be added to platforms
with menues.

'''

class SettingsDialog(wx.Dialog):
    def __init__ (self, parent, ID, title):
        wx.Dialog.__init__(self, parent, ID, title, size=(480, 400))
        self.SetForegroundColour(wx.Colour(0, 0, 0))
        self.SetBackgroundColour(wx.Colour(255, 255, 255))

        self.CurrentApplication = wx.SpinCtrl(self, wx.ID_ANY,value='0', min=0, max=4)
        self.ModeSwitch = wx.SpinCtrl(self, wx.ID_ANY,value='0', min=0, max=3)
        self.PowerButton = wx.ToggleButton(self, wx.ID_ANY, "Power")
        self.SpeakerVolume = wx.SpinCtrlDouble(self, wx.ID_ANY, value='0.0', min=0.0, max=11.0)
        self.RadioFrequency = wx.SpinCtrlDouble(self, wx.ID_ANY, value='88.10', min=86.00, max=108.00)
        self.RemotePort = wx.SpinCtrl(self, wx.ID_ANY, value='0', min=0, max=99999)
        self.RemoteIP = wx.TextCtrl(self, wx.ID_ANY,"")
        self.RemoteTimeout = wx.SpinCtrlDouble(self, wx.ID_ANY, value='0.0', min=0.0, max=10.0)
        self.LoadButton = wx.Button(self, wx.ID_ANY, "Update")
        self.DefaultsButton = wx.Button(self, wx.ID_ANY, "Defaults")
        self.CancelButton = wx.Button(self, wx.ID_CANCEL, "Cancel")
        self.SaveButton = wx.Button(self, wx.ID_OK, "Save")

        self.__do_layout()

        self.Bind(wx.EVT_BUTTON, self.OnLoadButton, self.LoadButton)
        self.Bind(wx.EVT_BUTTON, self.OnDefaultsButton, self.DefaultsButton)
        self.Bind(wx.EVT_BUTTON, self.OnCancelButton, self.CancelButton)
        self.Bind(wx.EVT_BUTTON, self.OnSaveSettingsButton, self.SaveButton)
        # there needs to be a stow here.
        self.UpdateSettings(data=1)

    def __do_layout(self):
        self.SpeakerVolume.SetMinSize((350, 20))
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        sizer_5 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, "Save"), wx.HORIZONTAL)
        sizer_4 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, "Remote"), wx.HORIZONTAL)
        sizer_6 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, "Frequency"), wx.HORIZONTAL)
        sizer_3 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, "Power"), wx.HORIZONTAL)
        sizer_2 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, "Application Setting"), wx.HORIZONTAL)
        label_0 = wx.StaticText(self, wx.ID_ANY, "Current Application:", style=wx.ALIGN_RIGHT)
        sizer_2.Add(label_0, 0, wx.EXPAND, 0)
        sizer_2.Add(self.CurrentApplication, 0, 0, 0)
        label_4 = wx.StaticText(self, wx.ID_ANY, "Mode")
        sizer_2.Add(label_4, 0, 0, 0)
        sizer_2.Add(self.ModeSwitch, 0, 0, 0)
        sizer_1.Add(sizer_2, 1, wx.EXPAND, 0)
        sizer_3.Add(self.PowerButton, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 26)
        sizer_3.Add((20, 20), 0, wx.ALIGN_CENTER_VERTICAL | wx.EXPAND, 0)
        sizer_3.Add(self.SpeakerVolume, 0, wx.ALIGN_CENTER_VERTICAL | wx.EXPAND, 0)
        sizer_1.Add(sizer_3, 0, 0, 0)
        sizer_6.Add(self.RadioFrequency, 0, wx.ALIGN_CENTER | wx.EXPAND, 0)
        sizer_1.Add(sizer_6, 1, wx.EXPAND, 0)
        label_1 = wx.StaticText(self, wx.ID_ANY, "Port:", style=wx.ALIGN_RIGHT)
        sizer_4.Add(label_1, 0, wx.ALIGN_CENTER_VERTICAL, 0)
        sizer_4.Add(self.RemotePort, 0, wx.ALIGN_CENTER_VERTICAL, 0)
        label_2 = wx.StaticText(self, wx.ID_ANY, "IP:")
        sizer_4.Add(label_2, 0, wx.ALIGN_CENTER_VERTICAL, 0)
        sizer_4.Add(self.RemoteIP, 0, wx.ALIGN_CENTER_VERTICAL, 0)
        label_3 = wx.StaticText(self, wx.ID_ANY, "Timeout:")
        sizer_4.Add(label_3, 0, wx.ALIGN_CENTER_VERTICAL, 0)
        sizer_4.Add(self.RemoteTimeout, 0, wx.ALIGN_CENTER_VERTICAL, 0)
        sizer_1.Add(sizer_4, 1, wx.EXPAND, 0)
        sizer_5.Add(self.LoadButton, 0, wx.ALIGN_CENTER_VERTICAL | wx.LEFT, 10)
        sizer_5.Add(self.DefaultsButton, 0, wx.ALIGN_CENTER_VERTICAL | wx.LEFT, 10)
        sizer_5.Add(self.CancelButton, 0, wx.ALIGN_CENTER_VERTICAL | wx.LEFT, 10)
        sizer_5.Add(self.SaveButton, 0, wx.ALIGN_CENTER_VERTICAL | wx.LEFT, 10)
        sizer_1.Add(sizer_5, 2, wx.EXPAND, 0)
        self.SetSizer(sizer_1)
        self.Layout()
        # end wxGlade


    def OnCancelButton(self, event):
        # there needs to be a recover here.
        self.UpdateSettings(data=1)
        event.Skip() # send the event up to modal dialog.

    def OnDefaultsButton(self, event):
        config.loadDefaults()
        self.UpdateSettings(data=1)

    def OnLoadButton(self, event):
        self.ReadSettings(data=1)

    def OnSaveSettingsButton(self, event):
        print ("...saving setting values...")
        config['DEFAULT']['current.application']=str(self.CurrentApplication.GetValue())
        config['DEFAULT']['mode.switch']=str(self.ModeSwitch.GetValue())
        config['DEFAULT']['power.switch']=str(self.PowerButton.GetValue())
        config['radioApplication']['radio.frequency']=str(self.RadioFrequency.GetValue())
        config['DEFAULT']['speaker.volume']=str(self.SpeakerVolume.GetValue())
        config['DEFAULT']['remote.port']=str(self.RemotePort.GetValue())
        config['DEFAULT']['remote.ip']=str(self.RemoteIP.GetValue())
        config['DEFAULT']['remote.timeout']=str(self.RemoteTimeout.GetValue())
        event.Skip() # send the event up to modal dialog

    def SaveSettings(self,data):
        config.writeSettingsFile()

    def UpdateSettings(self,data):
        print ("...load setting values...")
        self.CurrentApplication.SetValue(config.getint('DEFAULT','current.application'))
        self.ModeSwitch.SetValue(config.getint('DEFAULT','mode.switch'))
        self.PowerButton.SetValue(bool(config.getint('DEFAULT','power.switch'))) #kludge for value error.
        self.RadioFrequency.SetValue(config.getfloat('radioApplication','radio.frequency'))
        self.SpeakerVolume.SetValue(config.getfloat('DEFAULT','speaker.volume'))
        self.RemotePort.SetValue(config.getint('DEFAULT','remote.port'))
        self.RemoteIP.SetValue(config.get('DEFAULT','remote.ip'))
        self.RemoteTimeout.SetValue(config.getfloat('DEFAULT','remote.timeout'))
        self.Update()

    def ReadSettings(self,data):
        config.loadDefaults()
        config.readSettingsFile()

'''-------------------------------------------------------------------RadioRadio
This is the wxApp.

'''

class RadioRadio(wx.App):
    def OnInit(self):

        self.framework = MusicBox(None, wx.ID_ANY, "")
        self.SetTopWindow(self.framework)

        self.framework.Show()

        if (args.config):
            settingsDialog=SettingsDialog(None,wx.ID_ANY,"Settings")
            settingsDialog.ShowModal()
            settingsDialog.Destroy()
            #print ("....show settings dialog here....")

        if (sys.platform == 'darwin'):
            '''
            on osx present controlls as seperate window.
            populate menues and keyboard shortcuts.
            '''
            print('....cool window stuff here....')
        else:
            if ThisSystemHasAlsa:
                print ("This system has alsa!!!")
                self.framework.setVolume=setAlsaVolume

            self.framework.ShowFullScreen(1,wx.FULLSCREEN_ALL)

        pub.sendMessage("radio.frequency", data=96.6)
        pub.sendMessage("power.switch", data=0)
        pub.sendMessage("mode.switch", data=0)
        pub.sendMessage("speaker.volume", data=0.0)


        self.framework.WarpPointer(0,0) # move the pointer off of the screen
        return True

    def OnExit(self):
        config.writeSettingsFile()
        print ("GoodBye!!\n")
        return 0



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", help="show settings dialog", action="store_true")
    args = parser.parse_args()
    NoiseWidget = RadioRadio(0)
    NoiseWidget.MainLoop()
