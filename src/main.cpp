//https://www.norwegiancreations.com/2015/10/tutorial-potentiometers-with-arduino-and-filtering/
#include <Arduino.h>
#include <Bounce2.h>
#include <TimeLib.h>

#define KNOB_RESOLUTION 14
#define ON_SWITCH_PIN   14
#define ON_MODE_PIN1    17
#define ON_MODE_PIN2    18
#define MUTE_AMP_PIN    10
#define FRQ_KNOB_PIN   A1
#define VOL_KNOB_PIN   A2
int isItOn=0;
unsigned long int minUpdateMs=500;
unsigned long int maxUpdateMs=2500;
unsigned long int lastUpdateMs=0;
unsigned long int lastTimeMs=0;
#define MAX_TIME_BUFFER_LENGTH 8
#define MAX_FLOAT_BUFFER_LENGTH 16

//longnames['KFRQ']='radio.frequency'
//longnames['SPWR']='power.switch'
//longnames['KVOL']='speaker.volume'
//longnames['SMOD']='mode.switch'
//longnames['SYNC']='sync.requested'
//longnames['ACKN']='sync.acknowledged'

#define KEYWORDS   "SYNC","ACKN","NACK","KFRQ","SPWR","KVOL","SMOD","NOPE"
enum keywordIndex {_SYNC_,_ACKN_,_NACK_,_KFRQ_,_SPWR_,_KVOL_,_SMOD_,_NOPE_};
#define NKEYWORDS  _NOPE_ + 1
const char *keywords[((NKEYWORDS)*4)+1] = { KEYWORDS };

int whichMode=0;
int ms1=0; // for bounce to work
int ms2=0; // these need to be here...
int frqKnobValue=0;
int volKnobValue=0;
time_t theNow=0;

Bounce onSwitch = Bounce();  // 10 ms debounce
Bounce mode1 = Bounce();  // 10 ms debounce
Bounce mode2 = Bounce();  // 10 ms debounce

/*-------------------------------- low pass filter from the internets --------*/

float V_EMA_a = 0.4;      //initialization of EMA alpha
int V_EMA_S = 0;          //initialization of EMA S
float F_EMA_a = 0.5;      //initialization of EMA alpha
int F_EMA_S = 0;          //initialization of EMA S

void setup() {
  pinMode(ON_SWITCH_PIN, INPUT_PULLUP);
  onSwitch.attach(ON_SWITCH_PIN);
  onSwitch.interval(10);
  pinMode(ON_MODE_PIN1, INPUT_PULLUP);
  mode1.attach(ON_MODE_PIN1);
  mode1.interval(10);
  pinMode(ON_MODE_PIN2, INPUT_PULLUP);
  mode2.attach(ON_MODE_PIN2);
  mode2.interval(10);
  // put your setup code here, to run once:
  Serial.begin(115200);
  analogReadResolution(KNOB_RESOLUTION);

  pinMode(MUTE_AMP_PIN, OUTPUT);

  isItOn=!onSwitch.read();
  digitalWrite(MUTE_AMP_PIN,isItOn?HIGH:LOW);

  ms1=!mode1.read();
  ms2=!mode2.read();

  F_EMA_S = round(map(analogRead(FRQ_KNOB_PIN),
                            (1<<KNOB_RESOLUTION)-1,0,
                            8600,10900
                          )/10.0
                        );
  //set EMA S for t=1
  V_EMA_S = map(analogRead(VOL_KNOB_PIN),
                     0,(1<<KNOB_RESOLUTION)-1,
                     5,115
                   );
}

void readKnobs(){
  int frqKnobRead = round(map(analogRead(FRQ_KNOB_PIN),
                            (1<<KNOB_RESOLUTION)-1,0,
                            8600,10900
                          )/10.0
                        );

  /*------------------------------ low pass filter from the internets --------*/
  frqKnobValue = F_EMA_S = (F_EMA_a*frqKnobRead) + ((1-F_EMA_a)*F_EMA_S);    //run the EMA

  int volKnobRead = map(analogRead(VOL_KNOB_PIN),
                     0,(1<<KNOB_RESOLUTION)-1,
                     1,115
                   );
  /*------------------------------ low pass filter from the internets --------*/
  volKnobValue = V_EMA_S = (V_EMA_a*volKnobRead) + ((1-V_EMA_a)*V_EMA_S);    //run the EMA

  if (volKnobValue<0) {
     volKnobValue=0;
   }
  if (volKnobValue>110) {
       volKnobValue=110 ;
  }

  if (mode2.update()) {
    ms2=!mode2.read();
  }
  whichMode = ms1+(ms2*2);
  if (onSwitch.update()) {
    isItOn = (!onSwitch.read());
    digitalWrite(MUTE_AMP_PIN,isItOn?HIGH:LOW);

  }

}
const char *modes[4]={"0","0","2","2"};

const char * timeStamp () {
  static char timeStampBuffer[9]="00000000";
  snprintf(timeStampBuffer,9,"%08X",(int)theNow);
  return timeStampBuffer;
}

const char * point1(float f){
  static char __floatBuffer[MAX_FLOAT_BUFFER_LENGTH]="0.0";
  snprintf(__floatBuffer,
         MAX_FLOAT_BUFFER_LENGTH,
         "%d.%d",
         int(f/10.0),
         int(f)%10
       );
       return __floatBuffer;
}

void update(enum keywordIndex index,const char *data){
  Serial.printf("%4s:%8s:%s\n",keywords[index],timeStamp(),data);
}
//#define UPDATE_MEBBY(X) (forceUpdate || ( (waited) && (X) ) )

void loop() {
  // read the input on analog pin 0:
   static float oldFrqKnobValue = frqKnobValue;
   static float oldVolKnobValue = volKnobValue;
   static int oldIsItOn=isItOn;
   static int oldWhichMode=whichMode;
   bool forceUpdate=false;
   bool waited=false;

   readKnobs();

   if (millis() <= lastUpdateMs) {
     lastTimeMs=lastUpdateMs=millis();
     forceUpdate=true; // rollover
   }
   if ((millis() - lastUpdateMs) > minUpdateMs) {
     waited=true; //
   } if ((millis() - lastUpdateMs) > maxUpdateMs) {
     forceUpdate=true;
     lastUpdateMs=millis();
   }

   //if (UPDATE_MEBBY(oldFrqKnobValue != frqKnobValue)){
   if (forceUpdate || ( (waited) && (oldFrqKnobValue != frqKnobValue)) ){
      update(_KFRQ_,point1(frqKnobValue));
      oldFrqKnobValue = frqKnobValue;
   }
   if (forceUpdate || ( (waited)  && (oldVolKnobValue != volKnobValue)) ) {
      update(_KVOL_,point1(volKnobValue));
      oldVolKnobValue = volKnobValue;
   }

   if (forceUpdate || ((waited) && (oldIsItOn!=isItOn))) {
     update(_SPWR_, isItOn ? "1":"0");
     oldIsItOn=isItOn;
   }
   if (forceUpdate || ((waited) && (oldWhichMode!=whichMode))) {
    update(_SMOD_,modes[whichMode]);
    oldWhichMode=whichMode;
   }
   if ((millis()-lastTimeMs)>999L) {
     theNow++;
     lastTimeMs=millis();
   }

   //lastTimeMs=lastUpdateMs=millis();

   delay(100);        // delay in between reads for stability
}
